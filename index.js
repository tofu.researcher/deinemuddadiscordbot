const http = require('http');
const fs = require('fs');
const os = require('os');
const Discord = require('discord.js');
const bot = new Discord.Client();
let TOKEN = null;
let WHOSLIST = null;
let WHOSSUBMITS = null;
const hostname = os.hostname();
const port = 27745;
var ar = [];
var submitsar = [];

fs.readFile("../deinemuddabotconfig.json", (err, data) => {
  if (err) {
    console.error(err.message);
  } else {
    init(JSON.parse(data));
  }
});


async function init(envvariables) {
  console.log(envvariables);
  TOKEN = envvariables.TOKEN;
  WHOSLIST = envvariables.WHOSLIST;
  WHOSSUBMITS = envvariables.WHOSSUBMITS;  

  console.log("TOKEN: " + TOKEN);

  bot.login(TOKEN);

  setTimeout(() => {
    var d = new Date();
    if (d.getHours == 5) {
      bot.destroy();
      bot.login(TOKEN);
    }
  }, 1000*60*60);  

  fs.readFile('./' + WHOSLIST, (err, data) => {
    if (err) {
      console.error(err.message);
    } else {
      ar = JSON.parse(data);
    }
  });

  fs.readFile('./' + WHOSSUBMITS, (err, data) => {
    if (err) {
      console.error(err.message);
    } else {
      submitsar = JSON.parse(data);
    }
  });
}

bot.on('ready', () => {
  console.info(`Logged in as ${bot.user.tag}!`);
});

bot.on('message', msg => {
  if (msg.content === 'ping') {
    msg.reply('pong');
    msg.channel.send('pong');

  } else if (msg.content.toLowerCase().startsWith('!wdw') || msg.content.toLowerCase().startsWith('!weisstduwer')) {
    msg.channel.send(`${getRandom()}`);

  } else if (msg.content.toLowerCase().startsWith('!wdw help') || msg.content.toLowerCase().startsWith('!weisstduwer help')) {
    msg.channel.send(`${getRandom()}`);

  } else {
    if (evalWords(msg.content.toLowerCase())) {
      msg.channel.send(`${getRandom()}`);

    } else if (msg.content.toLowerCase().includes("macht keinen spaß")) {
      msg.channel.send(`Weißt du wem das auch keinen Spaß macht? ${getRandom()}`);

    }
  }
});

function evalWords(w) {
  var triggers = ["weisst du wen", "weißt du wen", "weißt du was", "weisst du wem", "weißt du wem", "wissen wir wem", "wem denn", "kennst du wen"];
  for (var i = 0; i < triggers.length; i++) {
    if (w.includes(triggers[i]))
      return true;
  }
  if (w.includes('wer')) {
    let laufen = true; //solange laufen, laufen
    while (laufen) {
      const i = w.indexOf('wer');
      if ((i == 0 || w[i-1] === ' ') && ( i+3 <= w.length || w[i+3] === ' '))
        return true;
      w = w.substring(i+3, w.length);
      laufen = w.includes('wer');
    }
  }
  return false;
}



var rr = [];

function getRandom() {
  if (ar.length < 1) {
    ar = rr;
    rr = [];
  }
  const w = ar.splice(Math.floor(Math.random() * ar.length) % ar.length, 1);
  rr.push(w);
  return w;
}

const server = http.createServer((req, res) => {
  var url = req.url;
  res.setHeader('Access-Control-Allow-Origin', 'http://34.90.216.182:27745/');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  console.log(`${req.connection.remoteAddress}: ${req.method} - ${url}`);

  if (req.method == 'GET') {
    if (url === '/') {
      res.setHeader('Content-Type', 'text/html');
      fs.readFile('./index.html', (err, data) => {
        if (err) {
          res.statusCode = 404;
          res.end(err.message);
        } else {
          res.statusCode = 200;
          res.end(data);
        }
      });
    } else if (url === '/getRandom') {
      res.statusCode = 200;
      res.end(`${getRandom()}`);
    } else if (url === '/wdwd.css') {
      res.setHeader('Content-Type', 'text/css');
      fs.readFile('./wdwd.css', (err, data) => {
        if (err) {
          res.statusCode = 404;
          res.end(err.message);
        } else {
          res.statusCode = 200;
          res.end(data);
        }
      });
    } else if (url === '/wdwm.css') {
      res.setHeader('Content-Type', 'text/css');
      fs.readFile('./wdwm.css', (err, data) => {
        if (err) {
          res.statusCode = 404;
          res.end(err.message);
        } else {
          res.statusCode = 200;
          res.end(data);
        }
      });
    } else if (url === '/getSubmits') {
      res.setHeader('Content-Type', 'application/json');
      res.statusCode = 200;
      res.end(JSON.stringify(submitsar));
    } else if (url === '/getAlready') {
      res.setHeader('Content-Type', 'application/json');
      fs.readFile('./' + WHOSLIST, (err, data) => {
      if (err) {
        res.statusCode = 500;
        console.error(err.message);
      } else {
        res.statusCode = 200;
        res.end(data);
      }
  });
    } else {
      res.statusCode = 404;
      res.end(`Bad Request`);
    }
  } else if (req.method === 'POST') {
    if (url.split('/')[1] === 'submit' && url.split('/').length === 3) {
      var w = url.split('/')[2];
      if (!arrcontains(w, ar) && !arrcontains(w, submitsar)) {
        submitsar.push(w);
        fs.writeFile('./' + WHOSSUBMITS, JSON.stringify(submitsar), () => {
          res.statusCode = 200;
          res.end("OK");
        });
      } else {
        res.statusCode = 200;
        res.end("Den Vorschlag gab es bereits!");
      }
    }
  }
});

function arrcontains(w, ar) {
  for (var i = 0; i < ar.length; i++) {
    if (ar[i] === w) return true;
  }
  return false;
}

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
